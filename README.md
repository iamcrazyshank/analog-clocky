## Assignment 2 -> Mobile Application Design (Android) 
### Shashank Chandran (119225074)
## Task: 
1. Description: This work requires to develop 3 Activities. 
- Show a analog clock.
- Show a timer
- Setting screen

2. Technical Concepts in this project: -
- 	Design a clock with surface view.(Include a milisecond clock)
- 	Use a background thread for timer
- 	Use of shared preference for saving the user settings such as App theme, Hour and minutes hand colors.
- 	Make elegant design(UI/UX)

## Screenshots  :
![](/Screen_Shot_1.png "") 
![](/Screen_Shot_2.png "") 
![](/Screen_Shot_3.png "") 