package com.iamcrazyshank.analogclocky;
//Shashank Chandran
//UCC Student number : 119225074
//Importing all dependencies
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import java.util.Calendar;
import static android.content.Context.MODE_PRIVATE;

public class CustomAnalogClock extends View {
    private int mHeight, mWidth, miliWidth, miliHeight = 0;
    private int mPadding = 0;
    private int mNumeralSpacing = 0;
    private int mRadius = 0;
    private int miliRadius = 0;
    private Paint mPaint;
    private Rect mRect = new Rect();
    private Drawable background;
    private boolean isInit;
    private int[] mClockHours = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    //Using the saved shared Preferences
    SharedPreferences prefs = this.getContext().getSharedPreferences("ColourSettings",  MODE_PRIVATE);

    public CustomAnalogClock(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomAnalogClock(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!isInit) {
            initializeClock();
        }
        //fetching the themeColor(Tcolor) from shared Preferences
        String colour = prefs.getString("Tcolour", "WHITE");
        if(colour.equals("Dark")){
            //if dark , making the paint Dark grey
            canvas.drawColor(Color.DKGRAY);
            View parent=(View)this.getParent();
            parent.setBackgroundColor(Color.DKGRAY);
            View masterParent = (View) parent.getParent();
            masterParent.setBackgroundColor(Color.DKGRAY);
        }else if(colour.equals("Light")){
            //if light , making the paint whte
            canvas.drawColor(Color.WHITE);
            View parent=(View)this.getParent();
            parent.setBackgroundColor(Color.WHITE);
            View masterParent = (View) parent.getParent();
            masterParent.setBackgroundColor(Color.WHITE);
        }else {
            canvas.drawColor(Color.WHITE);
            View parent=(View)this.getParent();
            parent.setBackgroundColor(Color.WHITE);
            View masterParent = (View) parent.getParent();
            masterParent.setBackgroundColor(Color.WHITE);
        }
// function for  drawing the clock outer circle
        drawCircleBorder(canvas);
        //function for drawing the milisecond circle
        drawMiliCircleBorder(canvas);

        drawClockCenter(canvas);
        drawMiliClockCenter(canvas);

        //function for drawing the number on the border
        drawNumericHourBorder(canvas);

        drawMiniClockHands(canvas);
        drawClockHands(canvas);

        postInvalidateDelayed(500);
        invalidate();
    }

    private void initializeClock() {
        mPaint = new Paint();
        mHeight = getHeight();
        mWidth = getWidth();
        mPadding = mNumeralSpacing + 50; // spacing from circle border
        int minAttr = Math.min(mHeight, mWidth);
        mRadius = minAttr / 2 - mPadding;
        isInit = true;
    }

    private void drawCircleBorder(Canvas canvas) {
        mPaint.reset();
        String colour = prefs.getString("Tcolour", "WHITE");
        if(colour.equals("Dark")){
            mPaint.setColor(Color.parseColor("#0E0E0E"));
        }else if(colour.equals("Light")){
            mPaint.setColor(Color.parseColor("#f0eceb"));
        }else {
            mPaint.setColor(Color.parseColor("#b8b5b4"));
        }

        DashPathEffect dashPath = new DashPathEffect(new float[]{15,15}, (float)3.0);

        mPaint.setPathEffect(dashPath);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);

        mPaint.setAntiAlias(true);
        canvas.drawCircle(mWidth / 2, mHeight / 2, mRadius + mPadding - 10, mPaint);
    }



    private void drawClockCenter(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(mWidth / 2, mHeight / 2, 12, mPaint);
    }

    private void drawNumericHourBorder(Canvas canvas) {
        String colour = prefs.getString("Tcolour", "WHITE");
        if(colour.equals("Dark")){
            mPaint.setColor(Color.parseColor("#FFFFFF"));
        }else if(colour.equals("Light")){
            mPaint.setColor(Color.parseColor("#787170"));
        }else {
            mPaint.setColor(Color.parseColor("#E2182A"));
        }


        int fontSize = (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_SP, 16, getResources().getDisplayMetrics());
        mPaint.setTextSize(fontSize);
        for (int hour : mClockHours) {
            String tmp = String.valueOf(hour);
            mPaint.getTextBounds(tmp, 0, tmp.length(), mRect); // for circle-wise bounding
            double angle = Math.PI / 6 * (hour - 3); // as mathematical rule
            int x = (int) (mWidth / 2 + Math.cos(angle) * mRadius - mRect.width() / 2);
            int y = (int) (mHeight / 2 + Math.sin(angle) * mRadius + mRect.height() / 2);
            canvas.drawText(String.valueOf(hour), x, y, mPaint);
        }
    }

    private void drawClockHands(Canvas canvas) {
        Calendar calendar = Calendar.getInstance();
        float hour = calendar.get(Calendar.HOUR_OF_DAY);
        hour = hour > 12 ? hour - 12 : hour;
        drawHandLine(canvas, (hour + calendar.get(Calendar.MINUTE) / 60) * 5f, true, false,false);
        drawHandLine(canvas, calendar.get(Calendar.MINUTE), false, false,true);
        drawHandLine(canvas, calendar.get(Calendar.SECOND), false, true,false);
    }

    private void drawHandLine(Canvas canvas, double moment, boolean isHour, boolean isSecond, boolean isMinute) {
        double angle = Math.PI * moment / 30 - Math.PI / 2;
            int  handRadius = mRadius;

        if (isHour) {

            String colour = prefs.getString("Hcolour", "WHITE");
            if(colour.equals("Green")){
                mPaint.setColor(Color.GREEN);
            }else if(colour.equals("Red")){
                mPaint.setColor(Color.RED);
            }else if(colour.equals("Blue")){
                mPaint.setColor(Color.BLUE);
            }else if(colour.equals("Black")){
                mPaint.setColor(Color.BLACK);
            }else if(colour.equals("Yellow")){
                mPaint.setColor(Color.YELLOW);
            }else {
                mPaint.setColor(Color.YELLOW);
            }

            mPaint.setStrokeWidth(20);
            handRadius = mRadius - 180 ;
        }

        if (isMinute) {
            String colour = prefs.getString("Mcolour", "WHITE");
            if(colour.equals("Green")){
                mPaint.setColor(Color.GREEN);
            }else if(colour.equals("Red")){
                mPaint.setColor(Color.RED);
            }else if(colour.equals("Blue")){
                mPaint.setColor(Color.BLUE);
            }else if(colour.equals("Black")){
                mPaint.setColor(Color.BLACK);
            }else if(colour.equals("Yellow")){
                mPaint.setColor(Color.YELLOW);
            }else {
                mPaint.setColor(Color.YELLOW);
            }

            mPaint.setStrokeWidth(18);
             handRadius = mRadius - 80 ;
        }

        if (isSecond) {
            String colour = prefs.getString("Scolour", "WHITE");
            if(colour.equals("Green")){
                mPaint.setColor(Color.GREEN);
            }else if(colour.equals("Red")){
                mPaint.setColor(Color.RED);
            }else if(colour.equals("Blue")){
                mPaint.setColor(Color.BLUE);
            }else if(colour.equals("Black")){
                mPaint.setColor(Color.BLACK);
            }else if(colour.equals("Yellow")){
                mPaint.setColor(Color.YELLOW);
            }else {
                mPaint.setColor(Color.YELLOW);
            }
            mPaint.setStrokeWidth(7);
             handRadius = mRadius - 80 ;
        }
        canvas
                .drawLine(mWidth / 2, mHeight / 2, (float) (mWidth / 2 + Math.cos(angle) * handRadius),
                        (float) (mHeight / 2 + Math.sin(angle) * handRadius), mPaint);
    }


    private void drawMiliCircleBorder(Canvas canvas) {
        mPaint.reset();
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(4);
        mPaint.setAntiAlias(true);
        int minAttr1 = Math.min(mHeight, mWidth);
        miliRadius = minAttr1 / 9;
        miliWidth = (mWidth / 2)/2;
        miliHeight = mHeight / 2;
        canvas.drawCircle(miliWidth,miliHeight , miliRadius, mPaint);
    }

    private void drawMiliClockCenter(Canvas canvas) {
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawCircle(miliWidth, miliHeight, 7, mPaint);

    }

    private void drawMiniClockHands(Canvas canvas) {
        Calendar calendar = Calendar.getInstance();
        drawMiniHandLine(canvas, calendar.get(Calendar.MILLISECOND));
    }

    private void drawMiniHandLine(Canvas canvas, double moment) {

        double moment1 = moment / 16.88;
        double angle = Math.PI * moment1 / 30 - Math.PI / 2;
        int handRadius =  miliRadius - 50 ;
        String colour = prefs.getString("Tcolour", "WHITE");
        if(colour.equals("Dark")){
            mPaint.setColor(Color.WHITE);
        }else if(colour.equals("Light")){
            mPaint.setColor(Color.GRAY);
        }else {
            mPaint.setColor(Color.BLUE);
        }
        canvas.drawLine(miliWidth, miliHeight, (float) (miliWidth + Math.cos(angle) * handRadius),
                        (float) (miliHeight + Math.sin(angle) * handRadius), mPaint);
    }






}
