package com.iamcrazyshank.analogclocky;

//Shashank Chandran
//UCC Student number : 119225074

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private static Button Timer, Settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Timer = (Button) findViewById(R.id.timerBtn);
        Settings = (Button) findViewById(R.id.settingsBtn);


//intent to go to timer activity
        Timer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent GotoTimer=new Intent(MainActivity.this, TimerActivity.class);
                startActivity(GotoTimer);
            }
        });
//intent to go to settings activity
        Settings.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent GotoTimer=new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(GotoTimer);

            }
        });



    }
}
