package com.iamcrazyshank.analogclocky;
//Shashank Chandran
//UCC Student number : 119225074
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {

    //color array for saving the color preference
    String[] colors = { "Select a color","Red", "Black", "Green", "Blue", "Yellow" };
    //theme color array for saving the app theme preference
    String[] themeColor = { "Select a theme","Dark", "Light" };

    Spinner spnr1,spnr2,spnr3,spnr4;

    Button saveBtn,clrBtn;

    String HColor="",MColor="",TColor="",SColor="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        spnr1 = (Spinner)findViewById(R.id.spinner1);
        spnr2 = (Spinner)findViewById(R.id.spinner2);
        spnr3 = (Spinner)findViewById(R.id.spinner3);
        spnr4 = (Spinner)findViewById(R.id.spinner4);

        saveBtn = (Button)findViewById(R.id.savebutton);
        clrBtn = (Button)findViewById(R.id.clearbutton);

        //adapter for spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,colors);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spnr1.setAdapter(adapter);
        //adapter for spinner
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,colors);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spnr2.setAdapter(adapter1);
        //adapter for spinner
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,colors);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spnr3.setAdapter(adapter2);
        //adapter for spinner
        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,themeColor);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spnr4.setAdapter(adapter3);

        //Spinner action to save the color pref
        spnr1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getSelectedItem() == "Select a color"){

                }else{
                    HColor = parent.getItemAtPosition(position).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //Spinner action to save the color pref
        spnr2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getSelectedItem() == "Select a color"){

                }else{
                    MColor = parent.getItemAtPosition(position).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //Spinner action to save the color pref
        spnr3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getSelectedItem() == "Select a color"){

                }else{
                    SColor = parent.getItemAtPosition(position).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //Spinner action to save the color pref
        spnr4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getSelectedItem() == "Select a theme"){

                }else{
                    TColor = parent.getItemAtPosition(position).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //save button action for saving the color preferences in SharedPreferences "Color Settings"
        saveBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getSharedPreferences("ColourSettings", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();

                if (HColor != ""){
                    //saving hour hand color to Hcolor
                    editor.putString("Hcolour", HColor);


                }
                if (MColor != ""){
                    //saving min hand color to Mcolor
                    editor.putString("Mcolour", MColor);

                }
                if (SColor != ""){
                    //saving Sec hand color to Scolor
                    editor.putString("Scolour", SColor);

                }
                if (TColor != ""){
                    //saving theme color  to Tcolor
                    editor.putString("Tcolour", TColor);


                }
                Toast.makeText(SettingsActivity.this, "All preferences are saved!", Toast.LENGTH_SHORT).show();

                editor.commit();


            }
        });

        //button action to clear all the SharedPreferences
        clrBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //Display toast when clearing the Shared Pref
                Toast.makeText(SettingsActivity.this, "Clearing all saved preferences!", Toast.LENGTH_SHORT).show();
                SharedPreferences prefs = getSharedPreferences("ColourSettings", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.commit();



            }
        });

    }
}
